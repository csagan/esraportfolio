<?php 

    $showScrollToTop = false;

    include 'base.php'
    
?>

<?php startblock('portfolio-content') ?>

    <h2>Cognita</h2>
    <p class="type">Usability and A/B Testing (B2C)</p>
    <p>Testing the usability of specific journeys that parents take on the school website when deciding which school to send their children to, using both web & mobile prototypes.</p>
    <p>Coming soon... &#129321;&#129310;</p>
               
<?php endblock() ?>