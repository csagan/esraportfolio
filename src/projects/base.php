<?php 

    class SubProject {
        public $content;
        public $imgName;
        public $title;

        public function __construct($content, $imgName, $title) {
            $this->content = $content;
            $this->imgName = $imgName;
            $this->title = $title;
        }
    };

    $resourceRootPath = '../';

    $configs = include('../config.php');

    $raw_pass = isset($_POST['pass']) ? $_POST['pass'] : '';

    $pass = crypt($raw_pass, $configs['salt']);
       
    $isProtected = strpos($configs['protectedProjects'], basename($_SERVER['PHP_SELF'],'.php')) !== false;

    $isPageAuthorized = !$isProtected || $pass == $configs['password'];

    if(!isset($customPageStyleClass)) $customPageStyleClass = '';

    if(!isset($showScrollToTop)) $showScrollToTop = true;

    include '../base.php'; 
?>

<?php startblock('body') ?>

<link href="https://fonts.googleapis.com/css?family=PT+Serif&display=swap" rel="stylesheet">

<nav class="main-nav-outer" id="main">
    <div class="container">
        <ul class="main-nav nobinding">
            <li><a href="/">Back To Work</a></li>
        </ul>
        <a class="responsive-nav" href="#"><i class="fa-bars"></i></a>
    </div>
</nav>

<section class="main-section" data-custom-style-class="<?= $isPageAuthorized ? $customPageStyleClass : '' ?>">
    <div class="container portfolio-detail">
        <div class="row">
            <?php

                if($isPageAuthorized)
                {
                    emptyblock('portfolio-content');
                    
                    if($showScrollToTop)
                    {
            ?>
                        <div class="scroll-to-top hide">
                            <svg enable-background="new 0 0 32 32" height="32px" viewBox="0 0 32 32" width="32px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g><path d="M16,0C7.163,0,0,7.164,0,16c0,8.837,7.163,16,16,16c8.836,0,16-7.163,16-16C32,7.164,24.836,0,16,0z M16,30   C8.28,30,1.969,23.72,1.969,16C1.969,8.28,8.28,2,16,2c7.72,0,14,6.28,14,14C30,23.72,23.72,30,16,30z"/><path d="M16.699,11.293c-0.384-0.38-1.044-0.381-1.429,0l-6.999,6.899c-0.394,0.391-0.394,1.024,0,1.414   c0.395,0.391,1.034,0.391,1.429,0l6.285-6.195l6.285,6.196c0.394,0.391,1.034,0.391,1.429,0c0.394-0.391,0.394-1.024,0-1.414   L16.699,11.293z"/></g></svg>
                        </div>
            <?php
                    }
                }
                else
                {
                    if(isset($_POST))
                    {
                        $isPost = $_SERVER['REQUEST_METHOD'] == 'POST';
            ?>
                        <div class="password-protect-container center">
                            <span class="title">This page is protected.</span>
                            <form method="POST" action="">
                                <input class="form-control input-lg" placeholder="Password" type="password" name="pass" /><br>
                                <?php if($isPost) { ?>
                                <span class="invalid-password">Wrong password.</span><br>
                                <?php } ?>
                                <input class="btn btn-lg btn-soft-custom transition-3d-hover" type="submit" name="submit" value="OK" />
                            </form>
                        </div>
            <?php
                    }
                } 
            ?>
        </div>
    </div>
</section>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vanilla-lazyload/10.19.0/lazyload.min.js"></script>

<?php endblock() ?>