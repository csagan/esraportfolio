<?php 

    $customPageStyleClass = 'ethnographic-custom-style';

    include 'base.php' 

?>

<?php startblock('portfolio-content') ?>

    <h2>Ethnographic Research</h2>
    <p class="type">Field Research (B2B)</p>
    <p>Call Centre agents get calls and call many end customers back; for understanding end customers’ needs, providing solutions or setting appointments for field teams to visit them. The way they approach to the end customers in need, is maybe the most important aspect for the brand experience to be shaped in customers’ minds. It is a pivoting role for end customers to decide whether they should stay, buy additional services or leave the subject client company.</p>
    <br>
    <p>I conducted ethnographic field research in the aim of increasing call centre agents’ happiness, efficiency so that they can provide better customer experience.</p>
    <img class="lazy" src="../img/portfolio/ethnographic/efra_1.png" alt="Call Centre" />
    
    <h4>Goals</h4>
    <ul>
        <li>Discovering agents’ pain points that can open doors to new projects in order to support them for working happily, ensure end customers’ satisfaction and ensure loyalty of Call Centre’s Client Companies.</li>
        <li>Suggesting solutions for improving agents’ work experiences (digital UX and contextual suggestions)</li>
        <li>Exploring the wrong behaviours (especially interaction behaviours) agents do, that cause call center company tremendous financial loss.</li>
        <li>Informing the Call Centre and its owner Telecommunications company with Ethnographic Field Research Analysis.</li>
    </ul>

    <h4>Challenge</h4>
    <ul>
        <li>I had two field trips set for this research. Each one was one workday long. I had to conduct as many interviews as possible with different client companies’ operations’ agents in two complete workdays.</li>
    </ul>
    
    <p>A few examples of these client companies I interviewed agents of are ; Call Centre’s owner big telecommunications company, Turkish Airlines, 4th largest bank in Turkey, 3 government entities of Turkey.</p>
    <br>

    <h4>My Ethnographic Field Research Stages</h4>
    <p>I planned and conducted the whole research myself.</p>
    <br>
    <ul>
        <li>Defining aim of research.</li>
        <li>Determining branches to visit, arranging field trips.</li>
        <li>Determining people to talk to (inbound agents, outbound agents, instructors, team leaders, group leaders)</li>
        <li>Preparing interview questions.</li>
        <li>Taking photos from agents’ desks.</li>
        <li>Interviewing people. Sound recording interviews.</li>
        <li>Listening to hours of sound recording. Taking notes from records.</li>
        <li>Determining suggestions for improving agents’ work experiences (digital and contextual suggestions)</li>
        <li>Preparing Ethnographic Field Research Analysis.</li>
        <li>Presenting Ethnographic Field Research Analysis to managers of Call Centre.</li>
    </ul>

    <h4><b>1</b></h4>
    <h4><b>Defining aim of research.</b></h4>
    <p>Even though call centre agents are maybe the most important touchpoint of front line services, their everyday experiences and painpoints were not known by the call centre company.</p>
    <br>
    <p>My initial aim was to disseminate the findings and contextual information related with agents'everyday experiences in call centre; so that new actions could be determined for improving their happiness.</p>
    <img class="lazy" data-src="../img/portfolio/ethnographic/efra_2.png" alt="Defining aim of research" />

    <h4><b>2</b></h4>
    <h4><b>Determining branches to visit, arranging field trips.</b></h4>
    <p>There were many branches of call centre throughout the country. I needed to visit the branches that had the most important client company operations in them, according to call centre’s strategic plans.</p>
    <br>
    <p>We determined these branches with my manager according to operations information we gathered from all branches.</p>
    <br>
    <p>I arranged 2 day field trips for 2 branches in 2 different locations.</p>
    <img class="lazy" data-src="../img/portfolio/ethnographic/efra_3.png" alt="Determining branches to visit, arranging field trips" />

    <h4><b>3</b></h4>
    <h4><b>Determining people to talk to.</b></h4>
    <p>Throughout their lives in call centre, the people agents are in communication with are; their colleagues (Inbound and Outbound agents in their operation), instructors, team leaders, group leaders.</p>
    <img class="lazy" data-src="../img/portfolio/ethnographic/efra_4.png" alt="Determining people to talk to" />

    <h4><b>4</b></h4>
    <h4><b>Preparing interview questions.</b></h4>
    <p>According to my aim of research I defined main themes to delve into. I structured interview questions based on these themes.</p>
    <img class="lazy" data-src="../img/portfolio/ethnographic/efra_5.png" alt="Preparing interview questions" />

    <h4><b>5</b></h4>
    <h4><b>Taking photos from agents’ desks.</b></h4>
    <p>Fragments of their lives in their everyday space. There were different desk sizes in different operations.</p>
    <img class="lazy" data-src="../img/portfolio/ethnographic/efra_6.png" alt="Taking photos from agents’ desks" />

    <h4><b>6</b></h4>
    <h4><b>Interviewing people. Sound recording interviews.</b></h4>
    <p>I interviewed Inbound and Outbound agents, instructors, team leaders, group leaders in operations in their very desks, offices.</p>
    <br>
    <p>I conducted interviews with them to learn the painpoints and to learn the background stories of why somethings are the way they are currently.</p>
    <br>
    <p>Sound recorded interviews for not missing any details. Also took of some key web app screens.</p>
    <img class="lazy" data-src="../img/portfolio/ethnographic/efra_7.png" alt="Interviewing people. Sound recording interviews" />

    <h4><b>7</b></h4>
    <h4><b>Listening to hours of sound recording. Taking notes from records.</b></h4>
    <p>The part that took the most time. I listened to approximately 20 hours of sound record and took pages of notes.</p>
    <img class="lazy" data-src="../img/portfolio/ethnographic/efra_8.png" alt="Listening to hours of sound recording. Taking notes from records" />

    <h4><b>8</b></h4>
    <h4><b>Determining suggestions for improving agents’ work experiences (digital UX and contextual suggestions).</b></h4>
    <p>My suggestions were for agents’ easy use of web app screens and for their not making mistakes while they are interacting with screens.</p>
    <br>
    <p>Eg: Entering information, jumping in between web apps etc. during their calls with customers.</p>
    <br>
    <br>


    <h4><b>9</b></h4>
    <h4><b>Preparing Ethnographic Field Research Analysis</b></h4>
    <p>I analysed my notes from interviews. Prepared Ethnographic Field Research Analysis with findings from research, my suggestions for improving agents’ work experiences and explanatory background information of current situation, cases.</p>
    <img class="lazy" data-src="../img/portfolio/ethnographic/efra_9.png" alt="Preparing Ethnographic Field Research Analysis" />
    <img class="lazy" data-src="../img/portfolio/ethnographic/efra_10.png" alt="Preparing Ethnographic Field Research Analysis" />
    <img class="lazy" data-src="../img/portfolio/ethnographic/efra_11.png" alt="Preparing Ethnographic Field Research Analysis" />
    <img class="lazy" data-src="../img/portfolio/ethnographic/efra_12.png" alt="Preparing Ethnographic Field Research Analysis" />

    <h4><b>10</b></h4>
    <h4><b>Presenting Ethnographic Field Research Analysis to managers and a director of Call Centre</b></h4>
    <p>They were all ears. They participated with questions and decided to disseminate the analysis.</p>
    <img class="lazy" data-src="../img/portfolio/ethnographic/efra_13.png" alt="Presenting Ethnographic Field Research Analysis to managers and a director of Call Centre" />

    <h4>Results</h4>
    <ul>
        <li>Agents shared main points to be good at, which would help them to up their skills and jump to better pay operations. After this, agents shared that they had aims to accomplish and did not have to work as blindly as before.</li>
        <li>Field engineers became accustomed to writing what actions they took in clients’ addresses without using technical jargon. This helped agents answer the questions of automatically called clients of companies, confidently. Agents mentioned this was a major factor in increasing their self-confidence and the feeling that they were doing their job properly - they were not causing the clients to leave the customer companies.</li>
        <li>My interaction design change and behavioural change recommendations increased the average number of loyal customers by %12 within the year of new releases.</li>
    </ul>
    <br>

    <h4>Reflections</h4>
    <ul>
        <li>Managers and director presented the analysis in Customer Experience internal conference of Call Centre’s owner Telecommunications Company to other managers, directors.</li>
        <li>My manager told me that my analysis has been guidance for planning their actions to improve agent’s experiences.</li>
    </ul>

<?php endblock() ?>