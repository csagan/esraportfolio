<?php 

    $customPageStyleClass = 'isbank-custom-style';

    include 'base.php' 

?>

<?php startblock('portfolio-content') ?>

    <h2>İşbank</h2>
    <p class="type">Discovery Research & Service Design (B2C)</p>
    <h4>Ideal Journeys of Becoming a Customer</h4>
    <p>It’s no question there are a lot of ways a person can become a customer of a bank by choosing to use any service from a variety of services.</p>
    <p>İşbank, 1st international bank of Turkey with more than 17,5 million customers; wanted to define their strategy through best experience journeys a person can have when s/he is <b><u>becoming a customer from branch</u></b> or <b><u>becoming a customer from online channels</u></b>, by opening a personal account.</p>
    <img class="lazy" src="../img/portfolio/isbank/isbank_1.png" alt="İşbank" />
    
    <h4>Goals</h4>
    <ul>
        <li>Determining painpoints throughout the journey of users while they are becoming a customer from branch or from online channels.</li>
        <li>Determining short term digital product design actions with Bank.</li>
    </ul>

    <h4>Challenges</h4>
    <ul>
        <li>Too much data to read, understand, analyse in the short time; given the fact that Bank had worked with a company for ethographic research, before I started working.</li>
        <li>Getting to know a new team of UX/UI designers to work with.</li>
        <li>Not having so much room for ideating, because of the values and regulations the bank followed.</li>
    </ul>
    
    <h4>My Role as a UX Strategist</h4>
    <ul>
        <li>Learning aims, expectations of Stakeholders.</li>
        <li>Reading and Analysing all data Bank had, from prior customer research and from benchmark studies.</li>
        <li>Project Planning</li>
        <li>Experiencing how to become a customer by opening a personal account from a branch, myself.</li>
        <li>Experiencing how to become a customer by opening a personal account from current online channels. My observing their current button labels, interfaces and flows.</li>
        <li>Defining the stages a customer follows in their end to end becoming a customer from branch and becoming a customer from online channels journeys.</li>
        <li>Determining painpoints and successful parts of the current experiences.</li>
        <li>Prioritising the painpoints according to the user research data.</li>
        <li>Conducting an early ideation workshop with stakeholders.</li>
        <li>Ideating with the UX/UI team on creative solutions for new ideal experience journeys of becoming a customer. Defining products, behaviours of customer advisors to include in ideal journeys.</li>
        <li>Designing low-fi journey maps of <b><u>becoming a customer from branch</u></b> and <b><u>becoming a customer from online channels by opening a personal account</u></b>.</li>
        <li>Designing new conceptual products which were key elements of ideal customer experience journeys in low-fi.</li>
        <li>Getting UX/UI teams’ support on working with the final visualisation of journeys and styles of new products’ UI.</li>
        <li>Working with Bank in every step of the way, getting their approvals. (They had technical and regulatory obstacles.)</li>
    </ul>
    
    <img class="lazy" data-src="../img/portfolio/isbank/isbank_2.png" alt="My Role as a UX Strategist" />
    <img class="lazy" data-src="../img/portfolio/isbank/isbank_3.png" alt="My Role as a UX Strategist" />
    <img class="lazy" data-src="../img/portfolio/isbank/isbank_4.png" alt="My Role as a UX Strategist" />
    <img class="lazy" data-src="../img/portfolio/isbank/isbank_5.png" alt="My Role as a UX Strategist" />

    <h4>Results</h4>
    <ul>
        <li>Bank determined some of their short and long term action plans according to these journey maps.</li>
        <li>These action plans were presented along with these journey maps to responsible managers in the bank.</li>
    </ul>

<?php endblock() ?>