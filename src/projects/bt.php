<?php 

    $showScrollToTop = false;

    include 'base.php'
    
?>

<?php startblock('portfolio-content') ?>

    <h2>BT</h2>
    <p class="type">Discovery Research (B2B)</p>
    <p>Understanding the decision-making tendencies, behaviours, and needs of BT’s target SME segments for their ideal future journeys.</p>
    <p>Coming soon... &#129321;&#129310;</p>
               
<?php endblock() ?>