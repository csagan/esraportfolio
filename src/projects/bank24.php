<?php include 'base.php' ?>

<?php startblock('portfolio-content') ?>

    <h2>Bank 24</h2>
    <p class="type">Usability Research & Information Architecture & UX Design (B2C)</p>
    <p>Digital transformation of large Turkish state owned bank’s ATM, Bank 24.</p>
    <img class="lazy" src="../img/portfolio/bank24/bank24_1.png" alt="HalkBank's ATM" />
    
    <h4>Goals</h4>
    <ul>
        <li>Reduce customer complaints</li>
        <li>On top of all a distinguishing ATM experience to speak of</li>
    </ul>
    
    <h4>My Role</h4>
    <ul>
        <li>Learning Key Audiences</li>
        <li>Writing Use Case Scenarios</li>
        <li>Running Eye Tracking Usability Research with two team members</li>
        <li>Gathering Data for Usability Analysis</li>
        <li>Open - Closed Card Sorting & Information Architecture</li>
        <li>Designing New Interactions</li>
        <li>Placing Functions</li>
        <li>Designing Wireframes & Prototyping</li>
        <li>Creating Visual Concept & Animating</li>
        <li>Preparing Navigation Flow Maps with final UI</li>
        <li>Preparing Usability Issues Analysis for Development Team</li>
    </ul>
    
    <h4>1. Learning Key Audiences</h4>
    <p>Either customer's aspect or their numbers showed the key audiences.</p>
    <img class="lazy" data-src="../img/portfolio/bank24/bank24_2.png" alt="Learning Key Audiences" />
    
    <h4>2. Writing Use Case Scenarios</h4>
    <p>We first needed to evaluate the existing ATM’s usability.</p><br>
    <p>We have worked with customer for determining the completion of use cases they see at the core.</p><br>
    <p>Then I have turned these use cases into real life scenarios.</p>
    <img class="lazy" data-src="../img/portfolio/bank24/bank24_3.png" alt="Writing Use Case Scenarios" />
    
    <h4>3. Running Eye Tracking Usability Research with two team members</h4>
    <p>We have determined the test users. Retired and working customers, aged in between 28 and 62.
        <br>Halkbank arranged us an ATM in it's Headquarter. We found our test users by the help of an agency. Users came one after another to our one day long testing. We used their valuable time in two sections.
    </p>
    <br>
    <ol>
        <li>Usability Testing over scenarios</li>
        <li>A short survey, for us to evaluate the influence of ATM on them​​​​​​​​​​​​​​</li>
    </ol>
    <img class="lazy" data-src="../img/portfolio/bank24/bank24_4.png" alt="Running Eye Tracking Usability Research with two team members" />
    
    <h4>4. Gathering Data for Usability Analysis</h4>
    <p>We gathered the data collected (notes taken while the user is completing the tasks, survey results) and formed the analysis.</p>
    <img class="lazy" data-src="../img/portfolio/bank24/bank24_5.png" alt="Gathering Data for Usability Analysis" />
    
    <h4>5. Open - Closed Card Sorting & Information Architecture</h4>
    <p>I moderated open and closed card sorting with users and analysed the outcome. Many users had been placed some particular features under quite different head features. For that reason I duplicated these features in the final architecture to ease the finding.</p>
    <img class="lazy" data-src="../img/portfolio/bank24/bank24_6.png" alt="Open - Closed Card Sorting & Information Architecture" />
    
    <h4>6. Designing New Interactions</h4>
    <p>​​​​​​Showing long textual information on buttons on ATM’s has always been a problem. (ex: Small account no on account choosing buttons). Transacting by seeing and acting right upon information as well (information in one screen, confirmation of previously chosen information and action button on another screen).</p>
    <br>
    <p>Explored general rules of usage by new interaction design for all.</p>
    <img class="lazy" data-src="../img/portfolio/bank24/bank24_7.png" alt="Designing New Interactions" />
    
    <h4>7. Placing Features</h4>
    <p>I designated priority values according to both the the Bank’s most used features and the features they want more to be used.</p>
    <br>
    <p>The button values were revealed based on our prior user research for eight button ATMs. A human being’s phsical and visual ergonomi constraints, tendencies were at core.</p>
    <br>
    <p>While running the research we realised the fact that the user may think, act upon habitual understanding. This was one other thing that had effect on the results.</p>
    <br>
    <p>All the quantitative data gave user prefferance priority values for each button.</p>
    <img class="lazy" data-src="../img/portfolio/bank24/bank24_8.png" alt="Placing Features" />
    
    <h4>8. Designing Wireframes & Prototyping</h4>
    <p>Designed the wireframe with close to real user guiding messages on screens. Guided a friend for prototyping.
    Users’ reactions were well. Shared prototypes with customer.</p>
    <img class="lazy" data-src="../img/portfolio/bank24/bank24_9.png" alt="Designing Wireframes & Prototyping" />
    
    <h4>9. Creating Visual Concept & Animating</h4>
    <p>Created visual concepts. Shared them with customer by a live video as the screens with each visual concept were really being used.</p>
    <br>
    <p>Customer chose <span class="highlight-in-text">“the Living with it’s User Concept"</span>. This concept is not only visually pleasing but also useful. We all have hard time seeing what’s on the ATM screen because of day, night light. The interfaces’ colors are changing according to the light around. Moreover the ATM is living the same season with it’s users by slowly animating video backgrounds.</p>
    <br>
    <p>Due to the fact that this product is in development phase I am sorry to only share very limited work.</p>
    <img class="lazy" data-src="../img/portfolio/bank24/bank24_10.png" alt="Creating Visual Concept & Animating" />
    
    <h4>10. Preparing Navigation Flow Maps</h4>
    <p>I filled one room in Bank’s Headquarters full of Final Design Navigation Flow Maps. Credit Card, Debit Card, Not On Us, Prepaid, Retired, Cardless Transactions. Helped customer show off about our work together.</p>
    <img class="lazy" data-src="../img/portfolio/bank24/bank24_11.png" alt="Preparing Navigation Flow Maps" />
    
    <h4>11. Preparing Usability Issues Analysis for Development Team</h4>
    <p>From that time on I wanted to make sure that the development process was going as designed. Flows, animations, feedback messages were all on track. I did Beta UAT testing, prepared usability issues for development team and guided customer along the development process.</p>
    <img class="lazy" data-src="../img/portfolio/bank24/bank24_12.png" alt="Preparing Usability Issues Analysis for Development Team" />
    <img class="lazy" data-src="../img/portfolio/bank24/bank24_13.png" alt="Preparing Usability Issues Analysis for Development Team" />
    
    <h4>Result</h4>
    <p>The digital transformation of ATM is in development process.</p>
    <br>
    
    <h4>Reflections</h4>
    <p>My customer is proud of the outcome. They are presenting our presentations, approach to human centered ATM Design in any platform possible.</p>
    <br>
    <p>After I started leading the project they came the following three years. We have worked for three years more together.</p>
    <br>
    <p>I felt the trust my customer had in me from their conversation in project meetings. This was a matchless sensation!</p>
    <br>
    <p>It was an honour to be a part of this giant project. I cannot wait for this experience to live with us.</p>
    <img class="lazy" data-src="../img/portfolio/bank24/bank24_14.png" alt="Reflections" />
    <br>
    <h4 class="center">Hey! Don't forget your cash!</h4>
    <img class="center lazy" data-src="../img/portfolio/bank24/bank24_15.gif" alt="Reflections" />

<?php endblock() ?>