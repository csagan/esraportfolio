<?php 

    $showScrollToTop = false;

    include 'base.php'
    
?>

<?php startblock('portfolio-content') ?>

    <h2>Spotify</h2>
    <p class="type">Fan Monetisation Concept Testing (B2C)</p>
    <p>Assessing whether the selected features resonate with fans of independent artists.</p>
    <p>Coming soon... &#129321;&#129310;</p>
               
<?php endblock() ?>