<?php include 'base.php' ?>

<?php startblock('portfolio-content') ?>

    <h2>Memoriam</h2>
    <p class="type">Concept Testing (B2C)</p>
    <img class="lazy" src="../img/portfolio/memoriam/memoriam_1.png" alt="Memoriam" />
    
    <p>I am a future human specie enthusiast. Searching about human perception, interaction changing discoveries and technologies that will make us become our new selves.</p>
    <br>
    <p>The technologies that will enhance our abilities tomorrow will be mobile. Healing faster with origami robots we swallow, surfing the web with contact lenses, feeling another person’s feelings live with a bracelet and a helmet...</p>
    <br>
    <p>I wanted to explore future mobile tech device’s features today, with todays’ most common mobile tech device <span class="highlight-in-text">“smartphone”</span>.</p>
    
    <h4>Memories Can Be Recorded</h4>
    <p>In Columbia University, Denny Laboratuary 2016 a rat was genetically modified to express light-sensitive ion channels.</p>
    <br>
    <p>Scientists made the rat live a cozy, happy memory with warm soil under it’s feet, soft light, objects to hide behind, easy findable food hidden behind the objects. This happy memory was recorded by following induced neuron networks while the rat was experiencing the soon after past memory.</p>
    <br>
    <p>The next day scientists placed the rat in a white floored uncovered glass tank under bright white light. The rat had a plug like device with lamps on it’s head. At first the rat was shocked in the tank and was not moving at all, it’s eyes open. Was there anything in the whole world more scarier than an environment that does not have anything to hide behind. One scientist triggered the plug device with a switch. And the lights on the plug started to light on and off. How was the mouse now? Walking under the bright light, smelling itself, itching just like yesterday on happy grounds. After a short while the scientist turned off the switch. <b>The mouse turned back to it’s condition Zero again. Shocked, stay put, open eyes.</b></p>
    <img class="lazy" data-src="../img/portfolio/memoriam/memoriam_2.png" alt="Memories can be recorded!" />
    
    <h4>The Research</h4>
    <p>Generally people are having hard time imaginig future, guessing about things they have never thought before. Knowing that, to be able to make them talk over <span class="highlight-in-text">“What if there was a social platform you could record your memories and share with people?”</span> I  thought of designing a mobile platform social media app prototype that does this.</p>
    <br>
    <p>Memoriam is a conceptual social app that records it’s users memories.</p>
    <br>
    <p>Users are humans. There are humans’ memories in the app as well as animals’ and plants’.</p>
    <br>
    <ul>
        <li>Human</li>
        <li>Animal</li>
        <li>Plants</li>
    </ul>
    <p>By this app the human can exchange lives. S/he can become one other by living his/her/it’s memory. With all the same feelings and heart rate of the recorder.</p>
    <br>
    <p class="highlight-in-text">“In the morning I am my friend, by noon I am a lion,  at night I am a tree.”</p>
    <img class="lazy" data-src="../img/portfolio/memoriam/memoriam_3.png" alt="The Research" />
    
    <h4>Challenge</h4>
    <p>Tomorrow’s target audience for this mobile app would be today’s high school students up to 21 years of age. However I neither know as many people in these ages nor I have the permission to go to a school, ask if I can interview these people. Therefor I went to a reasonable range that I could reach.</p>
    <br>
    
    <h4>My Role</h4>
    <ul>
        <li>Determining Aim of Research</li>
        <li>Visioning How Memoriam Could Really Make Users Live Memories</li>
        <li>Determining Features, Functions, Content According to the Vision</li>
        <li>Designing MEMORIAM with detailed settings for interviewing users by using the app in front of them</li>
        <li>Prototyping</li>
        <li>Finding Users</li>
        <li>Interviewing Users</li>
        <li>Preparing Qualitative and Quantitative Analysis</li>
    </ul>
    
    <h4>1. Determining Aim of Research</h4>
    <p>The aims of research are as follows.</p>
    <br>
    <ul>
        <li>Learning how users see tech</li>
        <li>Collecting insights, from reactions for the app to memories</li>
        <li>Understanding their usage of them. Gathering their curiosities over memories and feelings within</li>
    </ul>
    <img class="lazy" data-src="../img/portfolio/memoriam/memoriam_4.png" alt="Determining Aim of Research" />
    
    <h4>2. Visioning How Memoriam Could Really Make Users Live Memories</h4>
    <p>There must be some initial requirements. A pair of custom made smart contact lenses, a magic fruit (to measure the tendency to put a foreign material in body) are needed to experience memories of others and record own selves'.</p>
    <br>
    <p>Headphones and some place to sit.</p>
    <br>
    <p>More requirements for recording your plant's memory and your pet's memory.</p>
    <br>
    <p>And these must be obtained from Memoriam shops (online & offline).</p>
    <img class="lazy" data-src="../img/portfolio/memoriam/memoriam_5.png" alt="Visioning How Memoriam Could Really Make Users Live Memories" />
    
    <h4>3. Determining Features, Functions, Content According to the Vision</h4>
    <p>Here comes the functions arising from fuzziness</p>
    <br>
    <ul class="margin-bottom-0">
        <li>The user can record his/her memory</li>
        <li>The user can auto record and share his/her auto recording memory live With a contact within the app</li>
        <li>The user can experience a memory of someone’s (a contact’s or a stranger’s) with a contact or contacts</li>
        <li>The memories can be shared publicly</li>
        <li>The user can open an anonymous account and upload an anonymous memory</li>
        <li>The user can have as many accounts as s/he desires</li>
        <li>By the help of special crackers and an eye band the user can record his/her pet’s memories</li>
        <li>By the help of a special liquid the user can record his/her plant’s memories</li>
        <li>Plant’s memories are automatically fastened for the user to be able to experience within human time perception</li>
    </ul> ​​​​​​​
    
    <h4>4. Designing Memoriam</h4>
    <p>Content, taxonomy, detailed settings and visuals.</p>
    <br>
    <p>To me bluish purple is the definition of dreams’ delusions. I thought they were the right fit for Memoriam.</p>
    <br>
    <p>I welcome you to mid fidelity UI.</p>
    <img class="lazy" data-src="../img/portfolio/memoriam/memoriam_6.png" alt="Designing Memoriam" />
    
    <h4>5. Prototyping</h4>
    <p>In Flinto</p>
    <img class="lazy" data-src="../img/portfolio/memoriam/memoriam_8.png" alt="Prototyping" />
    
    <h4>6. Finding Users</h4>
    <p>Seven users are from people who are the permanent pattern of streets. Shop owners, waiters. Non-tech people.</p>
    <br>
    <p>Seven of them are working in tech companies. Strategist, budget tracker, project manager, machine engineer, developers. Tech people.</p>
    <br>
    <p>Having solid knowledge about what today’s working class’s concerns and embraces on MEMORIAM, gives valuable insights to make it useful for all ages.</p>
    <img class="lazy" data-src="../img/portfolio/memoriam/memoriam_9.png" alt="Finding Users" />
    
    <h4>7. Interviewing Users</h4>
    <p>Interviewed some on phone and some in field.</p>
    <img class="lazy" data-src="../img/portfolio/memoriam/memoriam_10.png" alt="Interviewing Users" />
    
    <h4>8. Preparing Qualitative and Quantitative Analysis</h4>
    <p class="margin-bottom-10">The data gathered under these titles</p>
    <ul>
        <li>The Users Commonly</li>
        <li>Users' Favourite Memories</li>
        <li>Users' Memories They Want to Forget</li>
        <li>Reaction to Memoriam</li>
        <li>Usage of Memoriam</li>
        <li>Is Memoriam a LifeTime App?</li>
        <li>The Preference of Memory Types by Percentages of Male and Female</li>
    </ul>
    <p>Some Quotes of Users Remained in Mind</p>
    <div class="bordered-box">
        <p class="highlight">"Tech would be useful if people didn’t realize they were using it."</p>
        <br>
        <p class="highlight">"I would download MEMORIAM because I am curious about something I don’t believe."</p>
        <br>
        <p class="highlight">"You know how giving birth to a baby is painful for a woman, may be that plant feels a similar pain while blossoming."</p>
        <br>
        <p class="highlight">“From now on, being able to record memories I prefer, would be advantageous since the control is in my hands”</p>
    </div>
    <img class="lazy" data-src="../img/portfolio/memoriam/memoriam_11.png" alt="Preparing Qualitative and Quantitative Analysis" />
    <br>
    <h6>Thank you for scrolling and please do not hesitate to talk to me about MEMORIAM.</h6>

<?php endblock() ?>