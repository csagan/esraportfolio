<?php 

    $showScrollToTop = false;

    include 'base.php'
    
?>

<?php startblock('portfolio-content') ?>

    <h2>Google Voice</h2>
    <p class="type">Discovery Research (B2B)</p>
    <p>Understanding telephony use cases for target small businesses and the role of communication applications (email, chat, video) in these journeys.</p>
    <p>Coming soon... &#129321;&#129310;</p>
               
<?php endblock() ?>