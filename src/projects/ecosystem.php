<?php 

    $showScrollToTop = false;

    include 'base.php'
    
?>

<?php startblock('portfolio-content') ?>

    <h2>World Health Organization / World Economic Forum / Wellcome Trust Foundation</h2>
    <p class="type">Discovery Research (B2B)</p>
    <p>Discovering digital opportunities to find and disseminate accurate disease health information.</p>
    <p>Coming soon... &#129321;&#129310;</p>
               
<?php endblock() ?>