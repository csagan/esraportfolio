<?php 

    $customPageStyleClass = 'personas-custom-style';

    include 'base.php'
    
?>

<?php startblock('portfolio-content') ?>

    <h2>Personas</h2>
    <p class="type">Expert Report (B2B)</p>
    <p>The willingness of long established companies to learn and apply UX methods to build their own internal products is refreshing.</p>
    <br>
    <p>Their desire to implement UX methods shows that these companies are starting to think about improving the daily lives of their employees and helping them to fulfill their daily job responsibilities in the easiest, smoothest way possible.</p>
    <br>
    <p>The international pharmaceutical company Novartis was particularly interested in Persona creation. They wanted to determine who they were designing for correctly but did not know how. They also wanted to understand how exactly the attributes of a Persona design help guide design decisions.</p>
    <br>
    <p>I eliminated the unknowns about Personas with this project.</p>
    <img class="lazy" data-src="../img/portfolio/personas/personas_1.png" alt="Personas" />
    
    <h4>Goal</h4>
    <ul>
        <li>Providing an easy to understand Persona Creation guide, the product teams can use.</li>
    </ul>

    <h4>My Role as a Senior UX Researcher</h4>
    <ul>
        <li>Explaining "What a Persona is" and "The aim of designing personas."</li>
        <li>Correcting the wrong tendencies that companies have while designing Personas. Diving into "What a Persona is not."</li>
        <li>Providing how Personas can be used effectively.</li>
        <li>Sharing how exactly Personas help guide design decisions.</li>
        <li>Underlining the fact that Personas are just the beginning for designing human centred products. Highlighting the major UX methods that need to be completed after persona design.</li>
    </ul>

    <h4>1. Explaining "What a Persona is" and "The aim of designing personas.”</h4>
    <p>I started preparing the guide with definitions. A persona is the answer of "Who are we designing for?”. A fictional yet realistic description of a target user of the product.</p>
    <br>
    <p>I explained the aim of designing personas by sharing how individuals in product teams can behave if personas aren't designed.</p>
    <img class="lazy" data-src="../img/portfolio/personas/personas_2.png" alt="Personas" />

    <h4>2. Correcting the wrong tendencies that companies have while designing Personas. Diving into "What a Persona is not.”</h4>
    <p>According to my experience working in companies whose main income is not from providing digital services - say a pharmaceutical company, a telecom operator company etc. I've seen that they have wrong knowledge about Persona design. Thus they can create incorrect, not based on any research Personas.</p>
    <br>
    <p>I defined the wrong tendencies that companies have when designing personas, like below and explained why they are wrong.</p>
    <br>
    <ul class="dash">
        <li>Marketing Personas are not UX Personas</li>
        <li>Broad Personas are not enough for designing human centred products</li>
        <li>User Roles are not personas</li>
    </ul>
    <img class="lazy" data-src="../img/portfolio/personas/personas_3.png" alt="Personas" />

    <h4>3. Providing how Personas can be used effectively.</h4>
    <p>Sometimes, product teams forget the purpose of persona design and create more Personas than needed. I made sure to share the ideal number of Personas needed for a human-centered product design.</p>
    <br>
    <p>It is also important to not forget that the designed Personas can be outdated for the products that are being used for years and years. Updated personas result in a better UX process. I provided research findings showing how effective Personas are based on the frequency of Persona updates.</p>
    <img class="lazy" data-src="../img/portfolio/personas/personas_4.png" alt="Personas" />

    <h4>4. Sharing how exactly Personas help guide design decisions.</h4>
    <p>Exactly how Personas guides design decisions is often not something that comes to minds of companies. The general tendency when designing Persona is to fill under the very common titles such as Motivations, Frustrations, Hobbies for a Persona without thinking about neither the designed product nor the business purposes over that product.</p>
    <br>
    <p>I shared different Persona designs from different projects to show the titles of a Persona design shall be different based on what is being designed.</p>
    <br>
    <p>I summarised what information a Persona design should be at its core with 2 focal points. Personality Traits and Needs Traits. I explained how each Persona attributes that represent these two Traits help guide design decisions.</p>
    <img class="lazy" data-src="../img/portfolio/personas/personas_5.png" alt="Personas" />

    <h4>5. Underlining the fact that Personas are just the beginning for designing human centred products. Highlighting the major UX methods that need to be completed after persona design.</h4>
    <p>In the most summarised way, personas help us identify features and content that must be provided to the target users of the product we are designing.</p>
    <br>
    <p>For closure, I drew attention to what UX Methods had to follow Persona design. After designing Persona comes clear Feature Labels, a navigation model (Information Architecture) that matches the mental model of target users, and easy-to-understand Content Design.</p>
    <img class="lazy" data-src="../img/portfolio/personas/personas_6.png" alt="Personas" />

    <h4>Results</h4>
    <p>I presented this guide available to the teams responsible for product creation and development at Novartis. There was great interest and positive feedback.</p>
    <br>
    <p>The guide was also shared with other teams working in other Business Services.</p>
    <br>
    <h4>Reflection</h4>
    <p>Different teams conducted Persona workshops using this guide to create and improve Novartis' internal products.</p>

<?php endblock() ?>