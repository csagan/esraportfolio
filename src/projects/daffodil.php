<?php 

    $showScrollToTop = false;

    include 'base.php'
    
?>

<?php startblock('portfolio-content') ?>

    <h2>Facebook</h2>
    <p class="type">Discovery Research (B2C)</p>
    <p>Understanding users' pain points in their behaviour regarding Facebook's selected values and identifying design opportunities that will positively strengthen these values.</p>
    <p>Coming soon... &#129321;&#129310;</p>
               
<?php endblock() ?>