<?php include 'base.php' ?>

<?php startblock('portfolio-content') ?>

    <h2>Social EYE</h2>
    <p class="type">Contextual Inquiry & Information Architecture & UX Design (B2B)</p>
    <p>A B2B social media replying tool. Casting user company's customers’ comments from indicated social media platforms.</p>
    <img class="lazy" src="../img/portfolio/socialeye/socialeye_1.png" alt="Social Eye" />

    <h4>Goals</h4>
    <ul>
        <li>Building an easy B2B social media replying tool, taking potential customer's everyday replying tool usage habits seriously.</li>
        <li>Creating solutions to customer's everyday tool problems that make them both loose unnecessary time and cause them to execute wrong tasks.</li>
    </ul>
    <h4>Challenges</h4>
    <ul>
        <li>A Zero Km Project. No business analysis, no prior set of functions to work on.</li>
        <li>​​​​Not having chances to come together with potential customer to learn their business, hear their needs and watch what they do with the equivalent app they use today (It had to be one single etnographic research day).</li>
        <li>​​​​The potential customer’s wanting the new app to be integrated with their other customized systems and apps as much as possible so that they can open tickets to responsible other departments, for their customers' problems in those departments' major.</li>
        <li>My company’s wanting the new app to be their own exclusive app (so that they can put their name under it, sell to other companies) <span class="highlight-in-text">"SOMETHING TO CALL OUR OWN EXCLUSIVE SOCIAL MEDIA REPLYING APP"</span></li>
        <li>My company’s wanting to make use the new app by the potential customer at the soonest possible date.​​​​​​​</li>
    </ul>
    <h4>My Role</h4>
    <ul>
        <li>Etnographic Research & Learning the Business</li>
        <li>Forming Personas</li>
        <li>Collecting What Potential Customer May Share</li>
        <li>Benchmarking</li>
        <li>Wireframe and Features Rough Study</li>
        <li>Determining Features and Information Architecture</li>
        <li>Crafting Especial Features and Their Usages for Potential Customer (The potential customer's users were lead users who have been using their equivalent app for years. Their expectations lead the way for any other future potential customers.)</li>
        <li>Determining Use Cases</li>
        <li>Designing UI</li>
        <li>Clickable Prototyping with UI</li>
        <li>Demonstrating the Prototype to my Bosses and Project Team</li>
    </ul>
    
    <h4>1. Etnographic Research & Learning the Business</h4>
    <p>I visited to the Potential Customer Company’s (Turk Telekom: the first integrated telecommunications operator in Turkey with 19.6 million mobile subscribers) users who uses an equivalent app as their major tool for everyday shift.</p>
    <br>
    <p>I have recorded then listened all our interview with potential customer and took many notes regarding the business, usability issues and functions they use. I have to say this interview was pretty much of a business analysis itself.</p>
    <br>
    <p>Ran the interview session in two sessions:<p>
    <br>
    <p><b>1st session</b> was to learn their everyday work generally, the teams they collaborate with to solve their customers’ problems.</p>
    <br>
    <p><b>2nd session</b> was to learn how they use the equivalent app, how they solve the problems they face because of the app and for any other additional reason.</p>
    <img class="lazy" data-src="../img/portfolio/socialeye/socialeye_2.png" alt="Etnographic Research & Learning the Business" />
    
    <h4>2. Forming Personas</h4>
    <p>Personas are the very answer to <span class="highlight-in-text">"Who we are designing for?"</span>. Unreal target users with their history, desires and personality. Writing personas is a clear way to understand key audiences.</p>
    <br>
    <p>I figured that the users are either long time call center assistants or first time job seekers who mostly do not have a higher education degree. All young. I focused on these two target user groups. Interviewed similar background people for background details.</p>
    <br>
    <p>Here my personas include demographical and personal information, interest - usage of apps and devices, preference of time and background stories. Additionally Orhan refers to an admin user thus he has Pain Point Section.</p>
    <img class="lazy" data-src="../img/portfolio/socialeye/socialeye_3.png" alt="Forming Personas" />
    
    <h4>3. Collecting What Potential Customer May Share</h4>
    <p>Not many but very informative the files were.</p>
    <img class="lazy" data-src="../img/portfolio/socialeye/socialeye_4.png" alt="Collecting What Potential Customer May Share" />
    
    <h4>4. Benchmarking</h4>
    <p>Examined equivalent apps. Collected features and solutions.</p>
    <img class="lazy" data-src="../img/portfolio/socialeye/socialeye_5.png" alt="Benchmarking" />
    
    <h4>5. Wireframe and Features Rough Study</h4>
    <p>For fast UX analysis, I wrote, erased, added, subtracted, drew.</p>
    <img class="lazy" data-src="../img/portfolio/socialeye/socialeye_6.png" alt="Wireframe and Features Rough Study" />
    
    <h4>6. Determining Features & Information Architecture</h4>
    <p>Features I collected by benchmarking + my potential customers’ needs to save them from unnecessary extra work + motivational surprise features to cheer them up (gamyfying).</p>
    <br>
    <p>Determined the features for the most authorised user. A super admin without a log screen, and for what I named General User.</p>
    <br>
    <p>There are a lot more features and functions than the information architecture below, yet they work with light boxes or with links to other pages.</p>
    <img class="lazy" data-src="../img/portfolio/socialeye/socialeye_7.png" alt="Determining Features & Information Architecture" />
    
    <h4>7. Crafting Especial Experiences and Their Usages for Potential Customer</h4>
    <p>The users come to their everyday table, put their bags and start seeing the same app, answering customers right away. I wanted the users' everyday friend web app to warmly welcome them by cute messages (changing according to their performance data). Created an essence by giving them the choice to start replying from the customer message they want to answer.</p>
    <br>
    <p>Motivated them openly showing the number of answered messages in answering screen. Filling up the concern if they were answering enough messages in the day shift so that they can deserve their premiums.</p>
    <br>
    <p>With Performance menu, I showed how they were doing in the team and let them decide their next goal in job themselves like a SIMs game.</p>
    <br>
    <p>Designed the settings, authorization screen according to how these features would work.</p>
    <img class="lazy" data-src="../img/portfolio/socialeye/socialeye_8.png" alt="Crafting Especial Experiences and Their Usages for Potential Customer" />
    
    <h4>8. Determine Use Cases</h4>                  
    <p>The more I learned about the app the more use cases came along. I designed the states of screens according to these use cases.</p>
    <div class="bordered-box">
        <p class="highlight">Examples</p><br>
        <p class="highlight margin-bottom-5">"A user's Register Channel screen who has identified one channel, yet has not set up a VIP Customer message alarm."</p>
        <p class="highlight margin-bottom-5">"A user's Register Channel screen who has identified one channel, has set up a VIP Customer message alarm."</p>
        <p class="highlight margin-bottom-5">"A user's Register Channel screen who has identified two channels, yet has not set up a VIP Customer message alarm."</p>
        <p class="highlight">"A user's Registering Channel screen who has identified two channels, has set up a VIP Customer message alarm."</p>
    </div>
    
    <h4>9. Designing UI</h4>
    <p>White background spaces had a meaning throughout the app. They were where the user must focus and act upon.</p>
    <br>
    <p>Light color backgrounds with subtle classification, darker dim color texts, informing icons, photos and of course the soul of a social media app came together.</p>
    <img class="lazy" data-src="../img/portfolio/socialeye/socialeye_9.png" alt="Designing UI" />
    
    <h4>10. Clickable Prototyping With UI</h4>
    <p>Prototyped the whole app with UI (no state screens).</p>
    <br>
    
    <h4>11. Demonstrated the Prototype to Project Team</h4>
    <p>Made a demonstration starting from what this app is to our potential customer, to my bosses, project manager and developers.</p>
    <img class="lazy" data-src="../img/portfolio/socialeye/socialeye_10.png" alt="Demonstrated the Prototype to Project Team" />
    <img class="lazy" data-src="../img/portfolio/socialeye/socialeye_11.png" alt="Demonstrated the Prototype to Project Team" />
    <h4>Results</h4>
    <ul>
        <li>Business Analyst wrote the analysis directly from my design and the product was developed.</li>
        <li>The product was sold to various clients mostly from the telecommunications sector. The employees in the client companies started to use the application actively in a week, this showed that they did not require any learning.</li>
    </ul>
    <br>
    <h4>Reflections</h4>
    <p>I had easier processes ˆˆ. The blood in my veins fleow crazy especially while trying to understand the usage of the equivalent products, finding the meaning underneath with less communication in a short time. Most especially when I had expectations from myself to be proactively creative. I hope what I put on table tasted good.</p>
    <br>
    <div class="bordered-box center">
        <p class="highlight margin-bottom-5">After the demonstration my one friend in company said</p>
        <p class="highlight">“Yesterday was a day I realised we are doing good things in this company.”</p>
    </div>
               
<?php endblock() ?>