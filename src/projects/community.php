<?php 

    $showScrollToTop = false;

    include 'base.php'
    
?>

<?php startblock('portfolio-content') ?>

    <h2>Facebook</h2>
    <p class="type">Discovery and Concept Testing (B2C)</p>
    <p>Understanding how target segments use Facebook Groups and their expectations, ideas for enhancing the interaction experience in the future.</p>
    <p>Coming soon... &#129321;&#129310;</p>
               
<?php endblock() ?>