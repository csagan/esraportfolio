<?php 

    $customPageStyleClass = 'novartis-custom-style';

    include 'base.php';

    $subProjects = array(
        new SubProject('te-user-research-analysis', 'novartis_sub_project_2.png', 'User Research Analysis')
    );
?>

<?php startblock('portfolio-content') ?>
   
    <div class="sub-projects">
        <div class="title">
            <p>I planned and conducted user research for several intranet services in Novartis. Prepared quantitative and qualitative analysis presentations, shared users’ insights, needs, expectations with solution suggestions to stakeholders, managers. Here are two projects that you can have a look.</p>
        </div>
        <?php foreach($subProjects as $key=>$subProject) { ?>
            <div class="portfolio-box wow fadeInUp delay-0<?= $key+1 ?>s">
                <a href="#" data-content="<?= $subProject->content ?>">
                    <img class="img-thumbnail" src="../img/portfolio/thumbnail/<?= $subProject->imgName ?>" alt="">
                </a>
                <h3 class="title"><?= $subProject->title ?></h3>
            </div>
        <?php } ?>
    </div>

    <div class="close sub-project hide">
        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm6 16.538l-4.592-4.548 4.546-4.587-1.416-1.403-4.545 4.589-4.588-4.543-1.405 1.405 4.593 4.552-4.547 4.592 1.405 1.405 4.555-4.596 4.591 4.55 1.403-1.416z"/></svg>
    </div>

    <div data-sub-project="one-novartis-services" class="hide">

        <h2>One Novartis Services</h2>
        <p class="type">Discovery Research & Information Architecture (Employee experience)</p>
        <p>One intranet platform for multinational pharmaceutical company Novartis&#39;s employees. While
        providing explanatory information about HR, IT, TRAVEL, FINANCE etc. service groups, the platform
        also helps employees request sub-services of these service groups.</p>
        <img class="lazy" data-src="../img/portfolio/novartis/ons/novartis_1.png" alt="One Novartis Services" />

        <h4>Goals</h4>
        <ul>
            <li>Redesigning a useful, usable platform according to the findings in the analysis of Global User Research.</li>
            <li>Helping users solve their own HR, IT, TRAVEL, FINANCE etc. needs, so that they would not have
                to reach service desks for their resolvable necessities. Hence reduce the service desk costs.</li>
        </ul>
        
        <h4>Challanges</h4>
        <ul>
            <li>The current quantitative data’s being have collected in a confusing way. (e.g. the keyword data users typed
                frequently into the search bar of the platform seemed to be the same with the service group menu titles)</li>
            <li>Constricted time for planning, conducting, analysing user research and designing, iterating the design of the platform.</li>
            <li>Finding as many users from different countries as possible in this short time.</li>
            <li>Conducting remote multinational workshops and collecting data from them.</li>
        </ul>
        
        <h4>My Role as a Service Design Consultant</h4>
        <ul>
            <li>Learning the current service and aim from stakeholders</li>
            <li>Gathering analytics data indicating users’ current interaction with the platform</li>
            <li>Finding users to conduct user research with</li>
            <li>Deep Insights Research with one to one interviews</li>
            <li>Analysing Quantitative and Qualitative Data</li>
            <li>Defining Personas</li>
            <li>Conducting Onsite Workshop in Basel</li>
            <li>Conducting Two Remote Global Workshops</li>
            <li>Defining main design drivers</li>
            <li>Defining main category names and unique key pages to design</li>
            <li>Determining Information Architecture</li>
            <li>Designing the new service</li>
            <li>Gathering users’ insights about the home page and Iteration of Home Page</li>
            <li>Presenting the design to service owners, stakeholders, responsible managers related with development teams</li>
            <li>Documenting the design process</li>
            <li>Preparing User Journey map</li>
        </ul>
        <p>Note: We have carried out user research and design processes, as a team of two consultants.</p>
        <br>
        
        <h4>1. Learning The Current Service and Aim From Stakeholders</h4>
        <p>After a brief introduction to what the service is from stakeholders and owners, we explored:</p>
        <br>
        <p>What challenges do they have today?</p>
        <br>
        <p>Is there a target audience within the company?</p>
        <br>
        <p>What do they want this platform to achieve after the new design?</p>
        <br>
        <p>We used the platform ourselves to understand how the platform works currently, what content and features are available.</p>
        <img class="lazy" data-src="../img/portfolio/novartis/ons/novartis_2.png" alt="Learning the Current Service and Aim from Stakeholders" />
        
        <h4>2. Gathering Analytics Data Indicating Users’ Current Interaction with the Platform</h4>
        <p>We contacted with One Novartis Services Support Team for gathering analytics data of One Novartis Services Platform on</p>
        <br>
        <p>Data indicating how users get to the platform</p>
        <p>Most common link clicks / file downloads</p>
        <p>Most common word/words users use in search bar</p>
        <p>Most common search terms that return no results</p>
        <p>Most common unique page views</p>
        <p>Time spent on site</p>
        <p>Bounce rate</p>
        <p>Most common difficulties or improvement suggestions that users share</p>
        <img class="lazy" data-src="../img/portfolio/novartis/ons/novartis_3.png" alt="Gathering Analytics Data Indicating Users’ Current Interaction with the Platform" />
        
        <h4>3. Finding Users to Conduct User Research with</h4>
        <p>We were given a list of already volunteered users from around the world.
                And happily stakeholders supported our need for users from India and USA.</p>
        <img class="lazy" data-src="../img/portfolio/novartis/ons/novartis_4.png" alt="Finding Users to Conduct User Research with" />
        
        <h4>4. Deep Insights Research with One to One Interviews</h4>
        <p>We have conducted 29 user interviews in ten Novartis locations. We have conducted our user interviews in two main parts.</p>
        <br>
        <p>The first part was for learning how they first learnt the platform, how often they use the platform for what reasons, what
                experiences they had during their interaction with the platform, what were their overall needs and expectations from the platform.</p>
        <br>
        <p>The second part was for observing them while using the current platform. We asked <span class="highlight-in-text">“Would you please share your screen and walk me through
                how you find what you generally look for in One Novartis Services Platform?”</span>. The observation continued with more questions regarding their prior experiences with the
                platform. We focused on their most impactful memories.</p>
        <img class="lazy" data-src="../img/portfolio/novartis/ons/novartis_5.png" alt="Deep Insights Research with One to One Interviews" />
        <img class="lazy" data-src="../img/portfolio/novartis/ons/novartis_6.png" alt="Deep Insights Research with One to One Interviews" />

        <h4>5. Analysing Quantitative and Qualitative Data</h4>
        <p>The quantitative data we have gathered and the negative experiences users shared while they
                were expressing their thoughts, feelings, expectations when using the platform did let us transform
                their needs to clear need sentences.</p>
        <br>
        <p>We have prioritised primary needs.</p>
        <img class="lazy" data-src="../img/portfolio/novartis/ons/novartis_7.png" alt="Analysing Quantitative and Qualitative Data" />
        <img class="lazy" data-src="../img/portfolio/novartis/ons/novartis_8.png" alt="Analysing Quantitative and Qualitative Data" />
        <img class="lazy" data-src="../img/portfolio/novartis/ons/novartis_9.png" alt="Analysing Quantitative and Qualitative Data" />

        <h4>6. Defining Personas</h4>
        <p>Personas are the very answer to Who we are designing for? Example target users with their
                history, desires and personality. Writing personas is a clear way to understand key audiences.</p>
        <br>
        <p>The observations and interviews showed that there were two main personas.</p>
        <br>
        <p>These personas trust themselves when they are faced with ambiguity in a digital platform and
                their interaction behaviour were the key differentiating factors that helped me define them.</p>
        <br>
        <p>I have defined one more persona who I called <span class="highlight-in-text">“The Human Persona”</span> This is a persona who has
                human needs such as I need to see the search term to be visible in the search results.</p>
        <img class="lazy" data-src="../img/portfolio/novartis/ons/novartis_10.png" alt="Defining Personas" />
        
        <h4>7. Conducting Onsite Workshop in Basel</h4>
        <p>We ran two day co-design workshop in Basel with stakeholders and user participants from
                Switzerland and Germany according to the workshop agenda I planned.</p>
        <img class="lazy" data-src="../img/portfolio/novartis/ons/novartis_11.png" alt="Conducting Onsite Workshop in Basel" />

        <p>The co-design activities brought users together to propose solutions that could meet genuine user
                needs. The participants worked in groups, using various creative materials and methods to mock
                up their ideas. This resulted in many inventive solutions.</p>
        <p>All these games helped us to see a great One Novartis Services platform through users' eyes.</p>

        <h4>8. Conducting Two Remote Global Workshops</h4>
        <p>We ran two remote workshops covering Asia, Europe (non Switzerland) and USA. The workshops were consisted of the two parts below.</p>
        <img class="lazy" data-src="../img/portfolio/novartis/ons/novartis_12.png" alt="Conducting Two Remote Global Workshops" />
        <img class="lazy" data-src="../img/portfolio/novartis/ons/novartis_13.png" alt="Conducting Two Remote Global Workshops" />

        <h4>9. Defining Main Design Drivers</h4>
        <p>Before we started designing the platform we wanted to define main drivers according to user
                research findings. These drivers would guide us when we are designing unique key pages of the
                platform just like it would guide content authors and developers.</p>
        <img class="lazy" data-src="../img/portfolio/novartis/ons/novartis_14.png" alt="Defining Main Design Drivers" />

        <h4>10. Defining Main Category Names And Determining Unique Key Pages To Design</h4>
        <p>The terms HR and IT were easily understood and applied by users in the current platform. We
            decided to use these two terms as the way they are in our new design.
            We have chosen easy to understand, everyday terms that define other main service groups so that
            users can match the names with their needs through the platform.</p>
        <br>
        <p>Current One Novartis Services platform is a giant information center which has hundreds of pages. We
            needed to choose the key pages to design within the restricted time.</p>
        <br>
        <p>Main design drivers above, category names and users’ needs made us determine these unique pages.</p>
        <img class="lazy" data-src="../img/portfolio/novartis/ons/novartis_15.png" alt="Defining Main Category Names And Determining Unique Key Pages To Design" />

        <h4>11. Determining Information Architecture</h4>
        <p>We translated the user needs into menus, interactive fields and important shortcut links/information.</p>
        <br>
        <p>Here is our information architecture.</p>
        <img class="lazy" data-src="../img/portfolio/novartis/ons/novartis_16.png" alt="Determining Information Architecture" />

        <h4>12. Designing the New One Novartis Services Platform</h4>
        <p>We have balanced the priorly needed information &amp; interaction models with the secondary needs
            of users in the interface design. We did it by placing of the content and less colour usage.</p>
        <br>
        <p>Rough hand sketches , other sketches, wireframes and finally the design.</p>
        <img class="lazy" data-src="../img/portfolio/novartis/ons/novartis_17.png" alt="Designing the New One Novartis Services Platform" />

        <h4>13. Gathering Users’ Insights About the Home Page and Iteration of Home Page</h4>
        <p>Even though we had only 2 days before the date we would present the platform we wanted to take
            users’ first impressions and preferences about the homepage design.</p>
        <br>
        <p>We have iterated the homepage design after gathering users' insights.</p>
        <img class="lazy" data-src="../img/portfolio/novartis/ons/novartis_18.png" alt="Gathering Users’ Insights About the Home Page and Iteration of Home Page" />

        <h4>14. Presenting the Design to Service Owners, Stakeholders, Responsible Managers Related with Development Teams</h4>
        <p>An exciting day it was. After a fast summary of user needs we have presented the prototype of our
            new design to over 10 managers. When the ones in Basel have come to see our design
            suggestion face to face, the others connected virtually from Novartis’s sites all around the world.</p>
        <br>
        <p>They asked questions. Our answers must have satisfied them since we have got positive feedback from them after our presentation.</p>
        <img class="lazy" data-src="../img/portfolio/novartis/ons/novartis_19.png" alt="Presenting the Design to Service Owners, Stakeholders, Responsible Managers Related with Development Teams" />

        <h4>15. Documenting the Design Process</h4>
        <p>Not only we have prepared a detailed presentation with each step we took, we also emphasised
            the heuristic rules that must be followed during and after the implementation of the design process
            for all teams that are responsible from the development of the new experience.</p>
        <img class="lazy" data-src="../img/portfolio/novartis/ons/novartis_20.png" alt="Documenting the Design Process" />

        <h4>16. Preparing User Journey Map</h4>
        <p>I have designed a holistic journey map covering the current and the new platform. I have shown how
            the fields we have designed were the answers to users’ needs on the map. I have matched the
            fields of our design with the needs they were related with.</p>
        <img class="lazy" data-src="../img/portfolio/novartis/ons/novartis_21.png" alt="Preparing User Journey Map" />

        <h4>Results</h4>
        <p>Development teams started developing the new platform within the technical constraints of the
                tool they use. We have given expert usability guidance based on UX Principles, UX Laws,
                Heuristic Rules, research findings on users' perception and interaction behaviour.</p>
        <br>

        <h4>Reflections</h4>
        <p>It was a fast and exciting journey for me. Honestly this was the biggest scale user research I have
            ever conducted in my career. While running the research with multinational users I have decided to
            do somethings differently for similar scale, constricted time projects in future. I said to myself <span class="highlight-in-text">"Even
            though you have learnt that you can only run a 2 hour remote workshop with users for a platform
            that covers tens of services, try ways to make it a 3 hour one”</span>.</p>
        <br>
        <p>I was fascinated to see how employees of the company are motivated to participate for making
            services useful and easy to interact. Many users approved mine and my colleague&#39;s requests for
            being interviewed and participating to workshops.</p>
        <br>
        <p>Stakeholders, owners of the platform not only supported us when we were understanding the current
            platform but also participated in one of our workshops. They were eager to hear how users think and
            behave, and what their expectations were when we were presenting our user research analysis.</p>
        <br>
        <p>Here are some quotes from stakeholders and users after we presented the new design of the platform.</p>
        <div class="bordered-box">
            <p class="highlight-in-text">“A fantastic outcome and great to develop a mockup in close collaboration
                with end-users. Now, technical feasibility as well as content availability needs
                to happen, of course in close collaboration with the end-users.”</p>
            <br>
            <p>Head of Quality and Customer Management</p>
        </div>
        <div class="bordered-box">
            <p class="highlight-in-text">“I am happily surprised of the result.  In such a short period of time from gather the end user feedback to concrete result- For me, this design is
                useful, versatile, simple, pragmatic and additionally looks nice- covering the main points raised by  ‘End-Users’”</p>
            <br>
            <p>A Service Design Authority</p>
        </div>
        <div class="bordered-box">
            <p class="highlight-in-text">“I think it is a very clean, easy look, with simple navigation. I especially like the way you incorporated the field in the How to Guide page that shows how
                much time it takes for specific tasks, so users will know in advance before engaging into a task, i.e. doing a booking, doing an approval etc.”</p>
            <br>
            <p>SDL Workforce Digital Enablement</p>
        </div>
        <div class="bordered-box">
            <p class="highlight-in-text">“Excellent - Very user centric and intuitive to use – Addressing everything that I need: How to... for self-help, Top services which I may use regularly, Customisable favourites”</p>
            <br>
            <p>IT Svc Del Lead Application Technology</p>
        </div>
        <br>
        <p>Users' Quotes</p>
        <div class="bordered-box">
            <p class="highlight-in-text margin-bottom-5">“You really captured the the needs from the workshop”</p>
            <p class="highlight-in-text margin-bottom-5">“I like How to Guides being immediately visible”</p>
            <p class="highlight-in-text margin-bottom-5">“Search results page is definitely better. I feel so relaxed when I see those filters”</p>
            <p class="highlight-in-text margin-bottom-5">“I like that One Novartis Services wants to learn my keywords for the information that I want to find”</p>
            <p class="highlight-in-text margin-bottom-5">“Very clear categories”</p>
        </div>
    </div>

    <div data-sub-project="te-user-research-analysis" class="hide">
        <h2>Travel & Expenses User Research Analysis</h2>
        <p class="type">Discovery Research & Information Architecture (Employee experience)</p>

    <?php for ($i = 1; $i <= 64; $i++) { ?>
        <img class="lazy" data-src="../img/portfolio/novartis/teura/teura_<?= $i ?>.png" alt="Travel & Expenses User Research Analysis" />
    <?php } ?>

    </div>
<?php endblock() ?>