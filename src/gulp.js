'use strict';

var gulp = require("gulp");
var htmlmin = require('gulp-htmlmin');
var cleanCss = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var concatJs = require('gulp-concat');
var concatCss = require('gulp-concat-css');

// Gulp task to minify HTML files
gulp.task('pages', function() { 
	return gulp.src(['*.php'])
			   .pipe(htmlmin({
					collapseWhitespace: true,
					removeComments: true
				}))
				.pipe(gulp.dest('dist'));
});

// Gulp task to minify CSS files
gulp.task('styles', function () {
	return gulp.src('css/**/*.css')
			   .pipe(concatCss("all.css"))
			   .pipe(cleanCss())
			   .pipe(gulp.dest('dist/css'))
});

// Gulp task to minify JavaScript files
gulp.task('scripts', function() {
	return gulp.src('js/**/*.js')
			   .pipe(concatJs('all.js'))
			   .pipe(uglify())
			   .pipe(gulp.dest('dist/js'))
});

// Gulp task to minify all files
gulp.task('default', gulp.parallel('pages', 'styles', 'scripts'));