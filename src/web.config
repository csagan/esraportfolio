<?xml version="1.0" encoding="utf-8"?>
<configuration>
    <system.web>
        <httpRuntime enableVersionHeader="false" />
        <customErrors mode="On" redirectMode="ResponseRedirect">
        <error statusCode="404" redirect="404.html" />
      </customErrors>
    </system.web>

    <system.webServer>

        <httpProtocol>
            <customHeaders>
                <clear />
                <add name="X-Content-Type-Options" value="nosniff" />
            </customHeaders>
            <redirectHeaders>
                <clear />
            </redirectHeaders>
        </httpProtocol>

        <rewrite>
            <outboundRules>
                <!-- https://w3c.github.io/webappsec-referrer-policy/ -->
                <rule name="Set Referrer-Policy header for HTML files only" enabled="true">
                    <match serverVariable="RESPONSE_Referrer-Policy" pattern=".*" />
                    <conditions>
                        <add input="{REQUEST_URI}" pattern="\.html$" />
                    </conditions>
                    <action type="Rewrite" value="strict-origin-when-cross-origin" />
                </rule>
                <rule name="Set X-Frame-Options header for HTML files only">
                    <match serverVariable="RESPONSE_X_Frame_Options" pattern=".*" />
                    <conditions>
                        <add input="{REQUEST_URI}" pattern="\.html$" />
                    </conditions>
                    <action type="Rewrite" value="DENY" />
                </rule>
                <rule name="Set X-XSS-Protection header for HTML files only">
                    <match serverVariable="RESPONSE_X_XSS_Protection" pattern=".*" />
                    <conditions>
                        <add input="{REQUEST_URI}" pattern="\.html$" />
                    </conditions>
                    <action type="Rewrite" value="1; mode=block" />
                </rule>
                <rule name="Set X-UA-Compatible header for HTML files only">
                    <match serverVariable="RESPONSE_X_UA_Compatible" pattern=".*" />
                    <conditions>
                        <add input="{REQUEST_URI}" pattern="\.html$" />
                    </conditions>
                    <action type="Rewrite" value="IE=edge" />
                </rule>
            </outboundRules>
        </rewrite>

        <httpErrors errorMode="Custom">
            <remove statusCode="404"/>
            <error statusCode="404" path="404.html" responseMode="Redirect"/>
        </httpErrors>

        <urlCompression doDynamicCompression="true" doStaticCompression="true" />

        <staticContent>
            <clientCache cacheControlCustom="public" cacheControlMode="UseMaxAge" cacheControlMaxAge="1.00:00:00" />

            <remove fileExtension=".css" />
            <remove fileExtension=".eot" />
            <remove fileExtension=".html" />
            <remove fileExtension=".ico" />
            <remove fileExtension=".js" />
            <remove fileExtension=".json" />
            <remove fileExtension=".otf" />
            <remove fileExtension=".svg" />
            <remove fileExtension=".ttc" />
            <remove fileExtension=".ttf" />
            <remove fileExtension=".woff" />
            <remove fileExtension=".woff2" />
            <remove fileExtension=".xml" />
            <mimeMap fileExtension=".css" mimeType="text/css; charset=utf-8" />
            <mimeMap fileExtension=".eot" mimeType="application/vnd.ms-fontobject" />
            <mimeMap fileExtension=".html" mimeType="text/html; charset=utf-8" />
            <mimeMap fileExtension=".ico" mimeType="image/x-icon" />
            <mimeMap fileExtension=".js" mimeType="application/javascript; charset=utf-8" />
            <mimeMap fileExtension=".json" mimeType="application/json; charset=utf-8" />
            <mimeMap fileExtension=".otf" mimeType="font/opentype" />
            <mimeMap fileExtension=".svg" mimeType="image/svg+xml" />
            <mimeMap fileExtension=".ttc" mimeType="application/x-font-ttf" />
            <mimeMap fileExtension=".ttf" mimeType="application/x-font-ttf" />
            <mimeMap fileExtension=".woff" mimeType="application/font-woff" />
            <mimeMap fileExtension=".woff2" mimeType="font/woff2" />
            <mimeMap fileExtension=".xml" mimeType="application/xml; charset=utf-8" />
        </staticContent>

    </system.webServer>
</configuration>
