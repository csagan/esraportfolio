<?php include 'base.php';

    class MenuItem {
        public $tag;
        public $text;

        public function __construct($tag, $text) {
            $this->tag = $tag;
            $this->text = $text;
        }
    }

    class Project {
        public $projectPage;
        public $imgName;
        public $title;
        public $type;
        public $desc;

        public function __construct($projectPage, $imgName, $title, $type, $desc) {
            $this->projectPage = $projectPage;
            $this->imgName = $imgName;
            $this->title = $title;
            $this->type = $type;
            $this->desc = $desc;
        }
    };

    $menuItems = array(
        new MenuItem('work', 'Work'),
        new MenuItem('about', 'About'),
        new MenuItem('contact', 'Contact')
    );

    $projects = array(
        new Project('spotify.php',          'spotify.png',          'Spotify',                                                 'Fan Monetisation Concept Testing (B2C)',         'Assessing whether the selected features resonate with fans of independent artists'),
        new Project('footballmanager.php',  'football_manager.png', 'Football Manager',                                        'Discovery Research (B2C)',                       'Identifying opportunities to make the game appealing to non-player target segments'),
        new Project('bt.php',               'bt.png',               'BT',                                                      'Discovery Research (B2B)',                       'Understanding the decision-making tendencies, behaviours, and needs of BT’s target SME segments for their ideal future journeys'),
        new Project('telephony.php',        'telephony.png',        'Google Voice',                                            'Discovery Research (B2B)',                       'Understanding telephony use cases for target small businesses and the role of communication applications (email, chat, video) in these journeys'),
        new Project('googleshopping.php',   'google_shopping.png',  'Google Shopping',                                         'Discovery Research (B2B)',                       'Understanding the decision-making criteria and needs of target Marketing professionals when choosing and engaging Amazon for advertising services'),
        new Project('community.php',        'community.png',        'Facebook',                                                'Discovery and Concept Testing (B2C)',            'Understanding how target segments use Facebook Groups and their expectations, ideas for enhancing the interaction experience in the future'),
        new Project('cognita.php',          'cognita.png',          'Cognita',                                                 'Usability and A/B Testing (B2C)',                'Testing the usability of specific journeys that parents take on the school website when deciding which school to send their children to, using both web & mobile prototypes'),
        new Project('daffodil.php',         'daffodil.png',         'Facebook',                                                'Discovery Research (B2C)',                       'Understanding users’ pain-points in Facebook’s behaviour regarding selected values and identifying design opportunities that will positively strengthen these values'),
        new Project('ecosystem.php',        'ecosystem.png',        'WHO / WEF / Wellcome Trust',                              'Discovery Research (B2B)',                       'Discovering digital opportunities to find and disseminate accurate disease health information'),
        new Project('personas.php',         'personas.png',         'Personas',                                                'Expert Report (B2B)',                            'Creating a guide for Novartis on how to identify and use target personas effectively while determining design decisions'),
        new Project('bp.php',               'bp.png',               'bp',                                                      'Discovery & Hypothesis Research (B2B)',          'Assessing the viability of an EV-Pro 12-month contract for ride-hailing drivers, along with all the reasons'),
        new Project('novartis.php',         'novartis.png',         'Novartis',                                                'Discovery Research & IA (Employee experience)',  'Identifying the HR, IT, Travel, Procurement, etc., needs of Novartis employees, and defining solution suggestions that will bring innovation to internal digital services that they can easily use'),
        new Project('bank24.php',           'bank24.png',           'Halkbank',                                                'Usability Research & IA & UX Design (B2C)',      'Transforming the ATM channel usage experience to achieve the bank’s business objectives and increase user engagement'),
        new Project('socialeye.php',        'socialeye.png',        'Türk Telekom',                                            'Contextual Inquiry & IA & UX Design (B2B)',      'Creating a new social media replying tool as a revenue stream for my company by identifying the needs and expectations of potential clients’ customer representatives'),
        new Project('memoriam.php',         'memoriam.png',         'Personal Project',                                        'Concept Testing (B2C)',                          'Exploring users’ curiosity, emotions, and usage of their own memories as well as those of others'),
        new Project('isbank.php',           'isbank.png',           'Isbank',                                                  'Discovery Research & Service Design (B2C)',      'Designing ideal experience journeys for becoming a customer from a branch and online channels through discovering the current needs of the bank’s target user personas'),
        new Project('ethnographic.php',     'efra.png',             'AssisTT',                                                 'Field Research (B2B)',                           'Identifying opportunities to enhance call centre agents’ happiness and efficiency for improved end customer experience by understanding representatives’ job responsibilities and emotions')
    );

    $experience = date("Y") - 2008;
?>

<?php startblock('body') ?>

    <nav class="main-nav-outer" id="main">
        <div class="container">
            <ul class="main-nav">
                <?php foreach($menuItems as $key=>$menuItem) { ?>
                    <li><a href="#<?= $menuItem->tag ?>"><?= $menuItem->text ?></a></li>
                <?php } ?>
            </ul>
            <a class="responsive-nav" href="#"><i class="fa-bars"></i></a>
        </div>
    </nav>

    <div id="sections">
        <section class="main-section" id="work">
            <div class="container">
                <h2>Work</h2>
                <h6>Have a look at my work.</h6>
            </div>
            <div class="portfolio-container">
               <?php foreach($projects as $key=>$project) { ?>
                    <div class="portfolio-box">
                        <a href="projects/<?= $project->projectPage ?>"><img class="img-thumbnail" 
                           src="img/portfolio/thumbnail/<?= $project->imgName ?>" alt=""></a>
                        <h4 class="title"><?= $project->title ?></h4>
                        <p class="type"><?= $project->type ?></p>
                        <span class="multine-ellipsis" title="<?= $project->desc ?>"><?= $project->desc ?></span>
                    </div>
               <?php } ?>
            </div>
        </section>

        <section class="main-section" id="about">
            <div class="container">
                <div class="row">
                    <figure class="wow fadeIn me">
                        <img src="img/me.png" alt="">
                    </figure>
                    <div class="featured-work wow fadeInUp">
                        <h2>Esra Ayhan Sagan</h2>
                        <h3><strong>User Experience Researcher, Designer & Consultant</strong></h3>
                        <span class="context">I have been creating and improving how users and customers experience many products and services for over <?= $experience ?> years.
                        Through user research, I uncover the needs, expectations, thoughts and feelings of users. This leads me to propose
                        relevant, delightful, intuitive, and ultimately valuable solutions.<br><br>
                        The business benefits that emerge range from reduced costs and customer complaints, through to increased customer
                        satisfaction and engagement.</span>
                    </div>
                </div>
            </div>
        </section>

        <section class="main-section contact center" id="contact">
            <div class="container">
                <h2>I’d Love to Hear From You</h2>
                <br>
                <i class="fa-envelope"></i><a href="#" data-type='email' data-address="esraayhan" data-domain="gmail.com">Send Email</a>
                <br><br>
                <i class="fa-map-marker"></i><span>London, United Kingdom</span>
                <br><br>
                <a href="#" class="btn btn-lg btn-pill btn-soft-custom transition-3d-hover margin-right-5" data-type="link" data-link="resume/Esra_Ayhan_Sagan_CV.pdf">Resume (Pdf)</a>
                <a href="#" class="btn btn-lg btn-pill btn-soft-custom transition-3d-hover" data-type="link" data-link="resume/Esra_Ayhan_Sagan_CV.docx">Resume (Doc)</a>
                <br><br>
                <div class="wow fadeInUp delay-05s">
                    <ul class="social-link">
                        <li class="linkedin">
                            <a href="#" data-type="link" data-link="https://www.linkedin.com/in/esraayhan/"><i class="fa-linkedin"></i></a>
                        </li>
                        <li class="behance">
                            <a href="#" data-type="link" data-link="https://www.behance.net/esrayhan"><i class="fa-behance"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
    </div>

<?php endblock() ?>