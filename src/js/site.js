var $doc =  $(document),
    $win = $(window);

$doc.ready(function (e) {

  var sections = $('#sections > section'),
      locationHash = window.location.hash,
      currentSection = (locationHash !== '') ? $(locationHash) : sections.first();

  function scrollToTop(top) {
    $('html, body').stop().animate({
      scrollTop: top
    }, 1500, 'easeInOutExpo');
  }

  function toggleCustomPageClass() {
    var $mainSection = $('.main-section'),
        toggleClass = $mainSection.attr('data-custom-style-class');

    if(toggleClass) {
      $mainSection.toggleClass(toggleClass);
    }
  }

  function toggleScrollToTop() {
    $('.scroll-to-top').toggleClass('hide');
  }

  sections.addClass('hide wow fadeInDown');

  currentSection.removeClass('hide');

  $('#main').scrollToFixed();

  $('.responsive-nav').click(function () {
      $('.main-nav').slideToggle();
      return false;
  });

  var $subProjects = $('.sub-projects');

  if(!$subProjects.length) {
    toggleScrollToTop();
    toggleCustomPageClass();
  }

  $('.sub-projects a').click(function() {
    toggleScrollToTop();
    toggleCustomPageClass();
    $subProjects.toggle();
    $('.close.sub-project').toggleClass('hide');
    $('[data-sub-project="' + $(this).attr('data-content') + '"]').toggleClass('hide');
  });

  $('.close.sub-project').click(function() {
    toggleScrollToTop();
    toggleCustomPageClass();
    $subProjects.toggle();
    $('[data-sub-project]:not(.hide)').toggleClass('hide');
    $(this).toggleClass('hide');
  });

  $('a[data-type="link"]').click(function() {
    var win = window.open(),
        $this = $(this);

    win.opener = null;
    win.document.location = $this.attr('data-link');
  });

  $('a[data-type="email"]').click(function () {
    var $this = $(this);
    window.location.href = 'mailto:' + $this.attr('data-address') + '@' + $this.attr('data-domain');
  });

  var lazyLoadSelector = '.lazy';
  if($(lazyLoadSelector).length){
      var lazyLoad = new LazyLoad({
          elements_selector: lazyLoadSelector,
          callback_error: function(el) { 
            $(el).attr('src', '/img/no-image.svg');
          }
      });
  }

  $('.portfolio-detail .scroll-to-top').click(function () { scrollToTop(0); });
});

var wow = new WOW({
  animateClass: 'animated',
  offset: 100
});
wow.init();

$win.load(function () {

  var navLinks = $('.main-nav:not(.nobinding) li a');

  navLinks.click(function (event) {

      var $this = $(this);

      if(!$this.data('isActive')) {

        $('section:not(.hide)').addClass('hide animated');

        $($this.attr('href')).removeClass('hide');
        
        navLinks.data('isActive', false);

        navLinks.removeClass('active');

        $this.addClass('active');

        $this.data('isActive', true);

        if ($win.width() < 768) {
            $('.main-nav').hide();
        };
      }

      event.preventDefault();
  });
});

$.fn.extend({
  animateCss: function(animationName, callback) {
    var animationEnd = (function(el) {
      var animations = {
        animation: 'animationend',
        OAnimation: 'oAnimationEnd',
        MozAnimation: 'mozAnimationEnd',
        WebkitAnimation: 'webkitAnimationEnd',
      };

      for (var t in animations) {
        if (el.style[t] !== undefined) {
          return animations[t];
        }
      }
    })(document.createElement('div'));

    this.addClass('animated ' + animationName).one(animationEnd, function() {
      $(this).removeClass('animated ' + animationName);

      if (typeof callback === 'function') callback();
    });

    return this;
  }
});