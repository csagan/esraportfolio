#addin "nuget:?package=Cake.Gulp&version=1.0.0"
#tool "nuget:?package=GitVersion.CommandLine"

//////////////////////////////////////////////////////////////////////
// ARGUMENTS
//////////////////////////////////////////////////////////////////////

var target = Argument("target", "Default");

//////////////////////////////////////////////////////////////////////
// TASKS
//////////////////////////////////////////////////////////////////////

Task("Clean")
    .Does(() =>
    {
        CleanDirectories("dist");
    });

Task("Gulp")
    .Does(() =>
    {
        Gulp.Local.Execute(settings => 
        {
            settings.WithGulpFile("./gulp.js");
        });
    });

Task("Copy-Needed-Files")
    .Does(() =>
    {
        CopyFile("favicon.ico", "dist/favicon.ico");
        CopyFile("favicon.ico", "dist/favicon.png");
        CopyFile("robots.txt", "dist/robots.txt");
        CopyFile("index.php", "dist/index.php");
        CopyFile("ti.php", "dist/ti.php");
        CopyFile("base.php", "dist/base.php");
        CopyFile("config.php", "dist/config.php");
        CopyDirectory("css", "dist/css");
        CopyDirectory("img", "dist/img");
        CopyDirectory("fonts", "dist/fonts");
        CopyDirectory("projects", "dist/projects");
    });

//////////////////////////////////////////////////////////////////////
// TASK TARGETS
//////////////////////////////////////////////////////////////////////

Task("Default")
    .IsDependentOn("Clean")
    .IsDependentOn("Gulp")
    .IsDependentOn("Copy-Needed-Files");

//////////////////////////////////////////////////////////////////////
// EXECUTION
//////////////////////////////////////////////////////////////////////

RunTarget(target);